<?php

namespace Pabon\SarlaftClient\Exceptions;

use Exception;

final class InvalidResponseException extends Exception
{
}
