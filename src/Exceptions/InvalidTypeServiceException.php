<?php

namespace Pabon\SarlaftClient\Exceptions;

use Exception;

final class InvalidTypeServiceException extends Exception
{
}
