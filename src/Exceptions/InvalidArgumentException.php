<?php

namespace Pabon\SarlaftClient\Exceptions;

final class InvalidArgumentException extends \InvalidArgumentException
{
}
