<?php

namespace Pabon\SarlaftClient\Entities;

use Pabon\SarlaftClient\Carriers\SoapCarrier;
use Pabon\SarlaftClient\Helpers\Logger;

class Settings
{
    const WDSL = __DIR__.'/../../sources/GrupoJurad.wsdl';

    protected $location;
    protected $username;
    protected $password;
    /**
     * @var Logger
     */
    protected $logger;
    private $internalUser;

    protected $client;

    public function __construct(array $settings)
    {
        $this->location = $settings['location'];
        $this->username = $settings['username'];
        $this->password = $settings['password'];
        $this->internalUser = $settings['internalUser'] ?? null;
        $this->logger = $settings['logger'] ?? new Logger(null);
        $this->client = new SoapCarrier(self::WDSL, [
            'location' => $this->location(),
        ], $this);
    }

    public function location(): string
    {
        return $this->location;
    }

    public function username(): string
    {
        return $this->username;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function logger(): Logger
    {
        return $this->logger;
    }

    public function client(): SoapCarrier
    {
        return $this->client;
    }

    public function setClient(SoapCarrier $client): self
    {
        $this->client = $client;

        return $this;
    }
}
