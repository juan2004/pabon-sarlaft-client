<?php

namespace Pabon\SarlaftClient\Entities\RiskQuery;

abstract class RiskList extends RiskMatch
{
    protected $listSource;
    protected $felony;
    protected $lastBulletinDate;
    protected $conclusion;
    protected $related;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->listSource = $data['listSource'] ?? null;
        $this->felony = $data['felony'] ?? null;
        $this->lastBulletinDate = $data['lastBulletinDate'] ?? null;
        $this->conclusion = $data['conclusion'] ?? null;
        $this->related = $data['related'] ?? null;
    }

    /**
     * Parses the information for the provider response to this format.
     * @param $result
     * @return self
     */
    abstract public static function parse($result);

    public function toArray(): array
    {
        return array_merge(parent::toArray(), [
            'listSource' => $this->listSource,
            'felony' => $this->felony,
            'lastBulletinDate' => $this->lastBulletinDate,
            'conclusion' => $this->conclusion,
            'related' => $this->related,
        ]);
    }
}
