<?php

namespace Pabon\SarlaftClient\Entities\RiskQuery;

abstract class RiskNews extends RiskMatch
{
    protected $title;
    protected $felony;
    protected $date;
    protected $link;
    protected $related;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->title = $data['title'] ?? null;
        $this->felony = $data['felony'] ?? null;
        $this->date = $data['date'] ?? null;
        $this->link = $data['link'] ?? null;
        $this->related = $data['related'] ?? null;
    }

    /**
     * Parses the information for the provider response to this format.
     * @param $result
     * @return self
     */
    abstract public static function parse($result);

    public function toArray(): array
    {
        return array_merge(parent::toArray(), [
            'title' => $this->title,
            'felony' => $this->felony,
            'date' => $this->date,
            'link' => $this->link,
            'related' => $this->related,
        ]);
    }
}
