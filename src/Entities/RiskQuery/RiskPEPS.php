<?php

namespace Pabon\SarlaftClient\Entities\RiskQuery;

abstract class RiskPEPS extends RiskMatch
{
    /**
     * @var string
     */
    protected $jobTitle;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->jobTitle = $data['jobTitle'] ?? null;
    }

    /**
     * Parses the information for the provider response to this format.
     * @param $result
     * @return self
     */
    abstract public static function parse($result);

    public function toArray(): array
    {
        return array_merge(parent::toArray(), [
            'jobTitle' => $this->jobTitle,
        ]);
    }
}
