<?php

namespace Pabon\SarlaftClient\Entities\RiskQuery;

use Exception;
use PlacetoPay\Base\Entities\Status;
use Psr\Log\LoggerInterface;

final class Result
{
    const RESULT_WITH_INFO = 'Associated records found';
    const RESULT_WITHOUT_INFO = 'No associated information found';

    private $status;
    private $score;
    private $risk;
    private $riskData;

    public function __construct(int $score, bool $risk, array $riskData, Status $status)
    {
        $this->status = $status;
        $this->score = $score;
        $this->risk = $risk;
        $this->riskData = $riskData;
    }

    public function isSuccessful(): bool
    {
        return $this->status->status === Status::ST_OK;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function score(): ?int
    {
        return $this->score;
    }

    public function hasRisk(): ?bool
    {
        return $this->risk;
    }

    public function riskData(string $list = '')
    {
        if ($list) {
            return $this->riskData[$list] ?? [];
        }

        return $this->riskData;
    }

    public static function createSuccessfulResponse(int $score, bool $risk, array $riskData): self
    {
        $message = self::RESULT_WITHOUT_INFO;
        if (count($riskData['peps']) > 0 || count($riskData['news']) > 0 || count($riskData['lists']) > 0) {
            $message = self::RESULT_WITH_INFO;
        }

        return new static($score, $risk, $riskData, Status::quickOk('00', $message));
    }

    public static function loadFromException(Exception $exception, LoggerInterface $logger = null): self
    {
        if ($logger) {
            $logger->error('Cause', [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'trace' => json_encode($exception->getTrace()) ? $exception->getTrace() : $exception->getTraceAsString(),
            ]);
        }

        return new static(-1, false, [], Status::quickFailed('XE', $exception->getMessage()));
    }
}
