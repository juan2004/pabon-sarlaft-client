<?php

namespace Pabon\SarlaftClient\Entities\RiskQuery;

abstract class RiskMatch
{
    const MATCH_BY_ID = 'ID';
    const MATCH_BY_NAME = 'NAME';

    /**
     * If the match is given by identification or name.
     * @var string
     */
    protected $matchBy;
    /**
     * The percentage of trust on this information based on the accuracy of the information.
     * @var float
     */
    protected $trustPercent;

    /**
     * Document type provided by the list.
     * @var string
     */
    protected $documentType;
    /**
     * Document number provided by the list.
     * @var string
     */
    protected $document;

    /**
     * The name of the list that provides this information.
     * @var string
     */
    protected $listName;

    /**
     * Status of the person associated with this information.
     * @var string
     */
    protected $personStatus;

    /**
     * Status of the merchant associated with this information if it applies.
     * @var string
     */
    protected $merchantStatus;

    /**
     * @var string
     */
    protected $nationality;

    public function __construct(array $data)
    {
        $this->matchBy = $data['matchBy'] ?? null;
        $this->trustPercent = $data['trustPercent'] ?? null;
        $this->documentType = $data['documentType'] ?? null;
        $this->document = $data['document'] ?? null;
        $this->listName = $data['listName'] ?? null;
        $this->personStatus = $data['personStatus'] ?? null;
        $this->merchantStatus = $data['merchantStatus'] ?? null;
        $this->nationality = $data['nationality'] ?? null;
    }

    public function toArray(): array
    {
        return [
            'matchBy' => $this->matchBy,
            'trustPercent' => $this->trustPercent,
            'documentType' => $this->documentType,
            'document' => $this->document,
            'listName' => $this->listName,
            'personStatus' => $this->personStatus,
            'merchantStatus' => $this->merchantStatus,
            'nationality' => $this->nationality,
        ];
    }
}
