<?php

namespace Pabon\SarlaftClient\Entities\SearchBasicData;

use Exception;
use PlacetoPay\Base\Entities\Status;
use Pabon\SarlaftClient\Constants\Result;
use Psr\Log\LoggerInterface;

final class ResultBasicData
{
    private Status $status;
    private array $basicData;

    public function __construct(array $basicData, Status $status)
    {
        $this->status = $status;
        $this->basicData = $basicData;
    }

    public function isSuccessful(): bool
    {
        return $this->status->status === Status::ST_OK;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function basicData(): ?array
    {
        return $this->basicData;
    }

    public static function createSuccessfulResponse(array $data): self
    {
        $message = Result::RESULT_WITHOUT_INFO;
        if (isset($data['denomination']) || isset($data['idStatus'])) {
            $message = Result::RESULT_WITH_INFO;
        }

        return new static($data, Status::quickOk('00', $message));
    }

    public static function loadFromException(Exception $exception, LoggerInterface $logger = null): self
    {
        if ($logger) {
            $logger->error('Cause', [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'trace' => json_encode($exception->getTrace()) ? $exception->getTrace() : $exception->getTraceAsString(),
            ]);
        }

        return new static([], Status::quickFailed('XE', $exception->getMessage()));
    }
}
