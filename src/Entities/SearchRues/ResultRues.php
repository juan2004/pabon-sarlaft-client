<?php

namespace Pabon\SarlaftClient\Entities\SearchRues;

use Exception;
use PlacetoPay\Base\Entities\Status;
use Psr\Log\LoggerInterface;

final class ResultRues
{
    const RESULT_WITH_INFO = 'Associated records found';
    const RESULT_WITHOUT_INFO = 'No associated information found';

    private Status $status;
    private array $ruesData;

    public function __construct(array $ruesData, Status $status)
    {
        $this->status = $status;
        $this->ruesData = $ruesData;
    }

    public function isSuccessful(): bool
    {
        return $this->status->status === Status::ST_OK;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function ruesData(): ?array
    {
        return $this->ruesData;
    }

    public static function createSuccessfulResponse(array $riskData): self
    {
        $message = self::RESULT_WITHOUT_INFO;
        if ($riskData['enrollments']) {
            $message = self::RESULT_WITH_INFO;
        }

        return new static($riskData, Status::quickOk('00', $message));
    }

    public static function loadFromException(Exception $exception, LoggerInterface $logger = null): self
    {
        if ($logger) {
            $logger->error('Cause', [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'trace' => json_encode($exception->getTrace()) ? $exception->getTrace() : $exception->getTraceAsString(),
            ]);
        }

        return new static([], Status::quickFailed('XE', $exception->getMessage()));
    }
}
