<?php

namespace Pabon\SarlaftClient\Entities\SearchIdState;

use Exception;
use Pabon\SarlaftClient\Constants\Result;
use PlacetoPay\Base\Entities\Status;
use Psr\Log\LoggerInterface;

class ResultIdState
{
    private Status $status;
    private array $idState;

    public function __construct(array $idState, Status $status)
    {
        $this->status = $status;
        $this->idState = $idState;
    }

    public function isSuccessful(): bool
    {
        return $this->status->status === Status::ST_OK;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function idState(): ?array
    {
        return $this->idState;
    }

    public static function createSuccessfulResponse(array $data): self
    {
        $message = Result::RESULT_WITHOUT_INFO;
        if (isset($data['idStatus'])) {
            $message = Result::RESULT_WITH_INFO;
        }

        return new static($data, Status::quickOk('00', $message));
    }

    public static function loadFromException(Exception $exception, LoggerInterface $logger = null): self
    {
        if ($logger) {
            $logger->error('Cause', [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'trace' => json_encode($exception->getTrace()) ? $exception->getTrace() : $exception->getTraceAsString(),
            ]);
        }

        return new static([], Status::quickFailed('XE', $exception->getMessage()));
    }
}
