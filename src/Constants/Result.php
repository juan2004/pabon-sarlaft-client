<?php

namespace Pabon\SarlaftClient\Constants;

class Result
{
    public const RESULT_WITH_INFO = 'Associated records found';
    public const RESULT_WITHOUT_INFO = 'No associated information found';
}
