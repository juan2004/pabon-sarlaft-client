<?php

namespace Pabon\SarlaftClient;

use Exception;
use Pabon\SarlaftClient\Carriers\SoapCarrierMock;
use Pabon\SarlaftClient\Entities\RiskQuery\Result;
use Pabon\SarlaftClient\Entities\SearchBasicData\ResultBasicData;
use Pabon\SarlaftClient\Entities\SearchIdState\ResultIdState;
use Pabon\SarlaftClient\Entities\SearchRues\ResultRues;
use Pabon\SarlaftClient\Entities\Settings;
use Pabon\SarlaftClient\Exceptions\InvalidArgumentException;
use Pabon\SarlaftClient\Providers\Multiburo\MultiburoRequest;
use Pabon\SarlaftClient\Providers\Multiburo\RiskQuery\MultiburoRiskQueryResponse;
use Pabon\SarlaftClient\Providers\Multiburo\SearchBasicData\MultiburoSearchBasicDataResponse;
use Pabon\SarlaftClient\Providers\Multiburo\SearchIdState\MultiburoSearchIdStateRequest;
use Pabon\SarlaftClient\Providers\Multiburo\SearchIdState\MultiburoSearchIdStateResponse;
use Pabon\SarlaftClient\Providers\Multiburo\SearchRues\MultiburoSearchRuesRequest;
use Pabon\SarlaftClient\Providers\Multiburo\SearchRues\MultiburoSearchRuesResponse;
use PlacetoPay\Base\Helpers\DocumentHelper;

class Gateway
{
    /**
     * @var Settings
     */
    protected $settings;

    public function __construct(array $settings)
    {
        $this->settings = new Settings($settings);
    }

    public function riskQuery(array $data): Result
    {
        $client = $this->settings->client();
        try {
            $result = $client->makeCall('buscarSarlaf', MultiburoRequest::buildBody($data, $this->settings));

            return MultiburoRiskQueryResponse::parseResponse($result);
        } catch (Exception $exception) {
            return Result::loadFromException($exception, $this->settings->logger());
        }
    }

    public function searchRues(array $data): ResultRues
    {
        $client = $this->settings->client();
        try {
            $result = $client->makeCall('buscarRue', MultiburoSearchRuesRequest::buildBody($data, $this->settings));

            return MultiburoSearchRuesResponse::parseResponse($result);
        } catch (Exception $exception) {
            return ResultRues::loadFromException($exception, $this->settings->logger());
        }
    }

    public function searchBasicData(array $data): ResultBasicData
    {
        $client = $this->settings->client();
        try {
            $result = $client->makeCall('buscarDatosBasicos', MultiburoRequest::buildBody($data, $this->settings));

            return MultiburoSearchBasicDataResponse::parseResponse($result);
        } catch (Exception $exception) {
            return ResultBasicData::loadFromException($exception, $this->settings->logger());
        }
    }

    public function searchIdState(array $data): ResultIdState
    {
        $client = $this->settings->client();
        try {
            if ($data['documentType'] !== DocumentHelper::TYPE_CC) {
                throw new InvalidArgumentException('This service only accepts '.DocumentHelper::TYPE_CC.
                    ' as a type of document');
            }

            $result = $client->makeCall('buscarEstadoCedula', MultiburoSearchIdStateRequest::buildBody($data, $this->settings));

            return MultiburoSearchIdStateResponse::parseResponse($result);
        } catch (Exception $exception) {
            return ResultIdState::loadFromException($exception, $this->settings->logger());
        }
    }

    public function mock(): self
    {
        $this->settings->setClient(new SoapCarrierMock(Settings::WDSL, [], $this->settings));

        return $this;
    }
}
