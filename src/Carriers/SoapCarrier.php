<?php

namespace Pabon\SarlaftClient\Carriers;

use Pabon\SarlaftClient\Entities\Settings;
use SoapClient;
use SoapFault;

class SoapCarrier extends SoapClient
{
    private $arguments = [];
    private $headers = [];

    /**
     * @var Settings
     */
    private $settings;

    public function __construct($wsdl, array $options = [], Settings $settings = null)
    {
        $options = array_merge([
            'soap_version' => SOAP_1_1,
            'encoding' => 'UTF-8',
        ], $options);
        $this->settings = $settings;

        parent::__construct($wsdl, $options);
    }

    public function __doRequest($request, $location, $action, $version, $one_way = 0): ?string
    {
        $this->settings->logger()->debug('QUERY', [
            'method' => $action,
            'request' => trim($request),
        ]);

        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    public function makeCall(string $method, $arguments = [], $headers = [])
    {
        if ($headers) {
            $this->addHeaders($headers);
            $this->__setSoapHeaders($this->headers);
        }

        $this->arguments = array_merge($this->arguments, $arguments);

        try {
            $response = $this->$method($this->arguments);
        } catch (SoapFault $exception) {
            $this->settings->logger()->error('ERROR', [
                'message' => $exception->getMessage(),
                'request' => trim($this->__getLastRequest() ?? ''),
                'response' => trim($this->__getLastResponse() ?? ''),
            ]);
            throw $exception;
        }

        $this->settings->logger()->debug('RESPONSE', [
            'method' => $method,
            'response' => $response,
        ]);

        return $response;
    }

    public function addHeader($header)
    {
        $this->headers[] = $header;

        return $this;
    }

    public function addHeaders(array $headers)
    {
        foreach ($headers as $header) {
            $this->addHeader($header);
        }

        return $this;
    }

    public function addArgument($argument)
    {
        $this->arguments = array_merge($this->arguments, $argument);

        return $this;
    }
}
