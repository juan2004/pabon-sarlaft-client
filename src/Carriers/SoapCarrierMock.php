<?php

namespace Pabon\SarlaftClient\Carriers;

class SoapCarrierMock extends SoapCarrier
{
    const SUCCESSFUL_RESPONSE_WITH_INFO = '1061111111';
    const SUCCESSFUL_RESPONSE_WITHOUT_INFO = '1061111112';
    const AUTHENTICATION_FAILED_RESPONSE = '1061111113';
    const USER_INACTIVE_RESPONSE = '1061111114';
    const BAD_STRUCTURE_RESPONSE = '1061111115';
    const INCORRECT_VALUE_RESPONSE = '1061111116';

    private $arguments = [];
    private $headers = [];

    public function __doRequest($request, $location, $action, $version, $one_way = 0): ?string
    {
        $method = explode('/', $action);

        preg_match('/<ns1:NumeroIdentificacion>(.*?)<\/ns1:NumeroIdentificacion>/', $request, $matchIdNumberTag);
        preg_match('/[0-9]{4,11}/', $matchIdNumberTag[0], $matchNumber);

        switch ($matchNumber[0]) {
            case self::SUCCESSFUL_RESPONSE_WITH_INFO:
                switch (end($method)) {
                    case 'buscarSarlaf':
                        return  $this->getResponseFile('SearchSarlaf/with_info_response.xml');
                    case 'buscarEstadoCedula':
                        return $this->getResponseFile('SearchIdState/with_info_response.xml');
                    case 'buscarRue':
                        return $this->getResponseFile('SearchRues/with_info_response.xml');
                    case 'buscarDatosBasicos':
                        return $this->getResponseFile('SearchBasicData/with_info_response.xml');
                }

                break;
            case self::INCORRECT_VALUE_RESPONSE:
                switch (end($method)) {
                    case 'buscarSarlaf':
                        return $this->getResponseFile('SearchSarlaf/incorrect_response.xml');
                    case 'buscarEstadoCedula':
                        return $this->getResponseFile('SearchIdState/incorrect_response.xml');
                    case 'buscarRue':
                        return $this->getResponseFile('SearchRues/incorrect_response.xml');
                    case 'buscarDatosBasicos':
                        return $this->getResponseFile('SearchBasicData/incorrect_response.xml');
                }

                break;
            case self::BAD_STRUCTURE_RESPONSE:
                switch (end($method)) {
                    case 'buscarSarlaf':
                        return $this->getResponseFile('SearchSarlaf/bad_structure_response.xml');
                    case 'buscarEstadoCedula':
                        return $this->getResponseFile('SearchIdState/bad_structure_response.xml');
                    case 'buscarRue':
                        return $this->getResponseFile('SearchRues/bad_structure_response.xml');
                    case 'buscarDatosBasicos':
                        return $this->getResponseFile('SearchBasicData/bad_structure_response.xml');
                }

                break;
            case self::AUTHENTICATION_FAILED_RESPONSE:
                switch (end($method)) {
                    case 'buscarSarlaf':
                        return $this->getResponseFile('SearchSarlaf/authentication_failed_response.xml');
                    case 'buscarEstadoCedula':
                        return $this->getResponseFile('SearchIdState/authentication_failed_response.xml');
                    case 'buscarRue':
                        return $this->getResponseFile('SearchRues/authentication_failed_response.xml');
                    case 'buscarDatosBasicos':
                        return $this->getResponseFile('SearchBasicData/authentication_failed_response.xml');
                }

                break;
            case self::USER_INACTIVE_RESPONSE:
                switch (end($method)) {
                    case 'buscarSarlaf':
                        return $this->getResponseFile('SearchSarlaf/user_inactive_response.xml');
                    case 'buscarEstadoCedula':
                        return $this->getResponseFile('SearchIdState/user_inactive_response.xml');
                    case 'buscarRue':
                        return $this->getResponseFile('SearchRues/user_inactive_response.xml');
                    case 'buscarDatosBasicos':
                        return $this->getResponseFile('SearchBasicData/user_inactive_response.xml');
                }

                break;
            default:
                switch (end($method)) {
                    case 'buscarSarlaf':
                        return $this->getResponseFile('SearchSarlaf/without_info_response.xml');
                    case 'buscarEstadoCedula':
                        return $this->getResponseFile('SearchIdState/without_info_response.xml');
                    case 'buscarRue':
                        return $this->getResponseFile('SearchRues/without_info_response.xml');
                    case 'buscarDatosBasicos':
                        return $this->getResponseFile('SearchBasicData/without_info_response.xml');
                }

                break;
        }
    }

    private function getResponseFile(string $string)
    {
        return file_get_contents(__DIR__.'/../../tests/Support/'.$string);
    }
}
