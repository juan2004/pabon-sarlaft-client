<?php

namespace Pabon\SarlaftClient\Providers\Multiburo;

use PlacetoPay\Base\Helpers\DocumentHelper;
use Pabon\SarlaftClient\Entities\Settings;

class MultiburoRequest
{
    public static function buildBody(array $search, Settings $settings): array
    {
        $DOCUMENT_TYPES = [
            DocumentHelper::TYPE_CC => 1,
            DocumentHelper::TYPE_NIT => 2,
            DocumentHelper::TYPE_CE => 3,
            DocumentHelper::TYPE_PPN => 5,
            DocumentHelper::TYPE_SSN => 8,
        ];

        return [
            'busqueda' => [
                'Usuario' => $settings->username(),
                'Password' => $settings->password(),
                'IdTipoIdentificacion' => $DOCUMENT_TYPES[$search['documentType'] ?? ''] ?? 6,
                'NumeroIdentificacion' => $search['document'] ?? null,
                'Denominacion' => $search['name'] ?? null,
            ],
        ];
    }
}
