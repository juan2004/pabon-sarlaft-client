<?php

namespace Pabon\SarlaftClient\Providers\Multiburo\SearchIdState;

use Pabon\SarlaftClient\Entities\SearchBasicData\ResultBasicData;
use Pabon\SarlaftClient\Entities\SearchIdState\ResultIdState;
use stdClass;

class MultiburoSearchIdStateResponse
{
    public static function parseResponse(stdClass $data): ResultIdState
    {
        return (new self())->buildResponse($data);
    }

    protected function buildResponse(stdClass $data): ResultIdState
    {
        $basicData = $this->getIdState($data->buscarEstadoCedulaResult);

        return ResultIdState::createSuccessfulResponse($basicData);
    }

    private function getIdState(stdClass $instance): array
    {
        $data = [];

        $status = $instance->Status ?? null;
        $errorCode = $instance->CodigoError ?? null;
        $errorMessage = $instance->MensajeError ?? null;
        $sourceStatus = $instance->EstadoFuente ?? null;
        $statusSourceLegend = $instance->EstadoFuenteLeyenda ?? null;
        $lastUpdateDate = $instance->FechaUltimaActualizacion ?? null;
        $denomination = $instance->Denominacion ?? null;
        $idStatus = $instance->EstadoCedula ?? null;

        $data['status'] = $status;
        $data['errorCode'] = $errorCode;
        $data['errorMessage'] = $errorMessage;
        $data['sourceStatus'] = $sourceStatus;
        $data['statusSourceLegend'] = $statusSourceLegend;
        $data['lastUpdateDate'] = $lastUpdateDate;
        $data['denomination'] = $denomination;
        $data['idStatus'] = $idStatus;

        return $data;
    }
}
