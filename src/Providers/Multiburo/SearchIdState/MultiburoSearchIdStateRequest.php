<?php

namespace Pabon\SarlaftClient\Providers\Multiburo\SearchIdState;

use Pabon\SarlaftClient\Entities\Settings;
use PlacetoPay\Base\Helpers\DocumentHelper;

class MultiburoSearchIdStateRequest
{
    public static function buildBody(array $search, Settings $settings): array
    {
        $DOCUMENT_TYPES = [
            DocumentHelper::TYPE_CC => 1,
        ];

        return [
            'busqueda' => [
                'Usuario' => $settings->username(),
                'Password' => $settings->password(),
                'IdTipoIdentificacion' => $DOCUMENT_TYPES[$search['documentType'] ?? ''] ?? 6,
                'NumeroIdentificacion' => $search['document'] ?? null,
                'Denominacion' => $search['name'] ?? null,
                'FechaCedulacion' => $search['documentIssueDate'] ?? null,
            ],
        ];
    }
}
