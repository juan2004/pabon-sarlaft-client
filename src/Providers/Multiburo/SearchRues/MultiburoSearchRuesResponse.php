<?php

namespace Pabon\SarlaftClient\Providers\Multiburo\SearchRues;

use Pabon\SarlaftClient\Entities\SearchRues\ResultRues;
use Pabon\SarlaftClient\Exceptions\InvalidResponseException;
use Pabon\SarlaftClient\Helpers\Logger;
use stdClass;

class MultiburoSearchRuesResponse
{
    public static function parseResponse(stdClass $data): ResultRues
    {
        return (new self())->buildResponse($data);
    }

    protected function buildResponse(stdClass $data): ResultRues
    {
        $response = $data->buscarRueResult;

        $ruesData = $this->getRuesData($response);

        return ResultRues::createSuccessfulResponse($ruesData);
    }

    private function getRuesData(stdClass $instance): array
    {
        $data = [];

        $status = $instance->Status ?? null;
        $errorCode = $instance->CodigoError ?? null;
        $errorMessage = $instance->MensajeError ?? null;
        $sourceStatus = $instance->EstadoFuente ?? null;
        $statusSourceLegend = $instance->EstadoFuenteLeyenda ?? null;
        $lastUpdateDate = $instance->FechaUltimaActualizacion ?? null;
        $enrollments = isset($instance->Matriculas) ? true : null;
        $enrollmentsResponse = $instance->Matriculas->MatriculaResponse ?? [];

        $data['status'] = $status;
        $data['errorCode'] = $errorCode;
        $data['errorMessage'] = $errorMessage;
        $data['sourceStatus'] = $sourceStatus;
        $data['statusSourceLegend'] = $statusSourceLegend;
        $data['lastUpdateDate'] = $lastUpdateDate;
        $data['enrollments'] = $enrollments ? ['enrollmentsResponse' => $this->parseEnrollment($enrollmentsResponse),
        ] : null;

        return $data;
    }

    private function parseEnrollment(array|stdClass $enrollments): array
    {
        return match (true) {
            $enrollments instanceof stdClass => MultiburoSearchRuesEnrollments::parse((array) $enrollments),
            is_array($enrollments) => $this->iterateEnrollments($enrollments)
        };
    }

    private function iterateEnrollments(array $enrollments): array
    {
        $data = [];
        foreach ($enrollments as $enrollment) {
            try {
                $data[] = MultiburoSearchRuesEnrollments::parse((array) $enrollment);
            } catch (\Throwable $e) {
                (new Logger())->error('Could not loop through result enrollments on query in Rues', [$e]);
                throw new InvalidResponseException($e);
            }
        }

        return $data;
    }
}
