<?php

namespace Pabon\SarlaftClient\Providers\Multiburo\SearchRues;

use Illuminate\Support\Collection;

class MultiburoSearchRuesEnrollments
{
    public static function parse($result): array
    {
        return [
            'identificationType' => $result['TipoIdentificacion'] ?? null,
            'identificationNumber' => $result['NroIdentificacion'] ?? null,
            'denomination' => $result['Denominacion'] ?? null,
            'chamberOfCommerce' => $result['Camara'] ?? null,
            'enrollment' => $result['Matricula'] ?? null,
            'enrollmentDate' => $result['FechaMatricula'] ?? null,
            'renewedDate' => $result['FechaRenovacion'] ?? null,
            'lastYearRenewed' => $result['UltimoAñoRenovado'] ?? null,
            'validityDate' => $result['FechaVigencia'] ?? null,
            'cancellationDate' => $result['FechaCancelacion'] ?? null,
            'organizationType' => $result['TipoOrganizacion'] ?? null,
            'companyType' => $result['TipoSociedad'] ?? null,
            'enrollmentCategory' => $result['CategoriaMatricula'] ?? null,
            'affiliate' => $result['Afiliado'] ?? null,
            'enrollmentState' => $result['EstadoMatricula'] ?? null,
            'beneficiaryLaw1429' => $result['BeneficiarioLey1429'] ?? null,
            'numberOfEmployees' => $result['CantidadEmpleados'] ?? null,
            'EconomicActivities' => self::auxiliaryParse(
                $result['Actividades']->ActividadesResponse ?? null
            ) ?? null,
            'legalRepresentatives' => self::auxiliaryParse(
                $result['RepresentantesLegales']->RepresentantesLegalesResponse ?? null
            ) ?? null,
        ];
    }

    /** separar la logica para parsear actividades y representantes y no tener espanenglish */
    private static function auxiliaryParse($info): array
    {
        $data = [];
        if ($info) {
            if (is_array($info) || $info instanceof Collection) {
                foreach ($info as $value) {
                    $data[] = (array) $value;
                }
            } else {
                $data[] = (array) $info;
            }
        }

        return $data;
    }
}
