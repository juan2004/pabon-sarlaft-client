<?php

namespace Pabon\SarlaftClient\Providers\Multiburo\SearchBasicData;

use Pabon\SarlaftClient\Entities\SearchBasicData\ResultBasicData;
use stdClass;

class MultiburoSearchBasicDataResponse
{
    public static function parseResponse(stdClass $data): ResultBasicData
    {
        return (new self())->buildResponse($data);
    }

    protected function buildResponse(stdClass $data): ResultBasicData
    {
        $basicData = $this->getBasicData($data->buscarDatosBasicosResult);

        return ResultBasicData::createSuccessfulResponse($basicData);
    }

    private function getBasicData(stdClass $instance): array
    {
        $data = [];

        $status = $instance->Status ?? null;
        $errorCode = $instance->CodigoError ?? null;
        $errorMessage = $instance->MensajeError ?? null;
        $sourceStatus = $instance->EstadoFuente ?? null;
        $statusSourceLegend = $instance->EstadoFuenteLeyenda ?? null;
        $lastUpdateDate = $instance->FechaUltimaActualizacion ?? null;
        $denomination = $instance->Denominacion ?? null;
        $idStatus = $instance->EstadoCedula ?? null;
        $personType = $instance->TipoPersona ?? null;

        $data['status'] = $status;
        $data['errorCode'] = $errorCode;
        $data['errorMessage'] = $errorMessage;
        $data['sourceStatus'] = $sourceStatus;
        $data['statusSourceLegend'] = $statusSourceLegend;
        $data['lastUpdateDate'] = $lastUpdateDate;
        $data['denomination'] = $denomination;
        $data['idStatus'] = $idStatus;
        $data['personType'] = $personType;

        return $data;
    }
}
