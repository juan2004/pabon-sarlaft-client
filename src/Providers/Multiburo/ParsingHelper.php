<?php

namespace Pabon\SarlaftClient\Providers\Multiburo;

use Pabon\SarlaftClient\Entities\RiskQuery\RiskMatch;

trait ParsingHelper
{
    protected static function parseMatch($match)
    {
        $MATCHES = [
            'ID' => RiskMatch::MATCH_BY_ID,
            'Nombre' => RiskMatch::MATCH_BY_NAME,
        ];

        return $MATCHES[$match] ?? $match;
    }

    protected static function cleanText($text)
    {
        if (! $text) {
            return;
        }

        return preg_replace('/\n\s*/', ' ', trim($text));
    }

    protected static function parseDate($date, $originFormat = 'Ymd')
    {
        if ($date) {
            $date = (\DateTime::createFromFormat($originFormat, $date))->format('Y-m-d');
        }

        return $date;
    }
}
