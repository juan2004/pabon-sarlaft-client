<?php

namespace Pabon\SarlaftClient\Providers\Multiburo\RiskQuery;

use Pabon\SarlaftClient\Entities\RiskQuery\Result;
use stdClass;

class MultiburoRiskQueryResponse
{
    public static function parseResponse(stdClass $data): Result
    {
        return (new self())->buildResponse($data);
    }

    protected function buildResponse(stdClass $data): Result
    {
        $response = $data->buscarSarlafResult;
        $riskData = $this->getRiskData($response);

        if (count($riskData['peps']) > 1 || count($riskData['lists']) > 1 || count($riskData['news']) > 15) {
            return Result::createSuccessfulResponse(100, true, $riskData);
        }

        return Result::createSuccessfulResponse(0, false, $riskData);
    }

    private function getRiskData(stdClass $instance): array
    {
        $data = [];

        $peps = isset($instance->PepsResponse->PepsResponseInfo) ? (array) $instance->PepsResponse->PepsResponseInfo : [];
        $lists = isset($instance->ListasResponse->ListasResponseInfo) ? (array) $instance->ListasResponse->ListasResponseInfo : [];
        $news = isset($instance->NoticiasResponse->NoticiasResponseInfo) ? (array) $instance->NoticiasResponse->NoticiasResponseInfo : [];

        $data['peps'] = $this->parsePEPS($peps);
        $data['lists'] = $this->parseLists($lists);
        $data['news'] = $this->parseNews($news);

        return $data;
    }

    private function parsePEPS(array $peps)
    {
        $data = [];
        foreach ($peps as $object) {
            $data[] = MultiburoRiskPEPS::parse((array) $object)->toArray();
        }

        return $data;
    }

    private function parseLists(array $lists)
    {
        $data = [];
        foreach ($lists as $object) {
            $data[] = MultiburoRiskList::parse((array) $object)->toArray();
        }

        return $data;
    }

    private function parseNews(array $news)
    {
        $data = [];
        foreach ($news as $object) {
            $data[] = MultiburoRiskNews::parse((array) $object)->toArray();
            if (count($data) > 20) {
                break;
            }
        }

        return $data;
    }
}
