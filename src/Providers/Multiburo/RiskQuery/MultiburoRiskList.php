<?php

namespace Pabon\SarlaftClient\Providers\Multiburo\RiskQuery;

use Pabon\SarlaftClient\Entities\RiskQuery\RiskList;
use Pabon\SarlaftClient\Providers\Multiburo\ParsingHelper;

class MultiburoRiskList extends RiskList
{
    use ParsingHelper;

    public static function parse($result): self
    {
        $trust = (float) str_replace('%', '', $result['PorcentajeCoincidencia'] ?? '');

        $data = [
            'matchBy' => self::parseMatch($result['Coincidencia'] ?? null),
            'trustPercent' => $trust,
            'documentType' => $result['ListasTipoIdentificacion'] ?? null,
            'document' => $result['ListasNroIdentificacion'] ?? null,
            'listName' => $result['NombreLista'] ?? null,
            // TODO: Parse this two status
            'personStatus' => $result['EstadoPersonaNatural'] ?? null,
            'merchantStatus' => $result['EstadoNitPersonaJuridica'] ?? null,
            'nationality' => $result['Nacionalidad'] ?? null,

            'listSource' => $result['OrigenLista'] ?? null,
            'felony' => $result['Delito'] ?? null,
            'lastBulletinDate' => self::parseDate($result['UltFechaBoletin'] ?? null),
            'conclusion' => $result['Conclusion'] ?? null,
            'related' => self::cleanText($result['RelacionadoCon'] ?? null),
        ];

        return new self($data);
    }
}
