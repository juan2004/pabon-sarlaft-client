<?php

namespace Pabon\SarlaftClient\Providers\Multiburo\RiskQuery;

use Pabon\SarlaftClient\Entities\RiskQuery\RiskNews;
use Pabon\SarlaftClient\Providers\Multiburo\ParsingHelper;

class MultiburoRiskNews extends RiskNews
{
    use ParsingHelper;

    public static function parse($result): self
    {
        $data = [
            'matchBy' => null,
            'trustPercent' => null,
            'documentType' => null,
            'document' => $result['NroIdentificacion'] ?? null,
            'listName' => $result['MedioFuente'] ?? null,
            // TODO: Parse this two status
            'personStatus' => $result['EstadoPersonaNatural'] ?? null,
            'merchantStatus' => $result['EstadoNitPersonaJuridica'] ?? null,
            'nationality' => $result['Nacionalidad'] ?? null,

            'title' => self::cleanText($result['TituloNoticia'] ?? null),
            'felony' => self::cleanText($result['TipoDelito'] ?? null),
            'date' => self::parseDate($result['Fecha'] ?? null, 'd/m/Y'),
            'link' => self::cleanText($result['LinkFuente'] ?? null),
            'related' => self::cleanText($result['MedioSeccion'] ?? null),
        ];

        return new self($data);
    }
}
