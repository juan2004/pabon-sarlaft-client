# Sarlaft SDK

## Installation

This SDK can be installed easily through composer, it is private so you maybe need to add the [repository information](https://dev.placetopay.com/repository) provided by Satis

`composer require placetopay/sarlaft-client`

## Usage (Multiburó provider)

* Create a new instance of the Multiburo, you can also provide `logger` and `client` (SoapClient of php) parameters. 
* user, password and identification are provided by Multiburó, more info https://www.grupojurad.com/                                                                                                              

```php
$settings = [
    'user' => '',
    'password' => '',
    'identification' => '',
    'client' => '', 
    'logger' => '', 
];

$gateway = new \PlacetoPay\SarlaftClient\Gateways\Multiburo($settings);

```

* Request for Sarlaft

```php
$sarlaftSearch = new PlacetoPay\SarlaftClient\SarlafSearchMultiburo([
    'idIdentificationType' => '',
    'identificationNumber' => '',
    'denomination' => '',
    'timeOut' => '', //optional
    'tag' => '', //optional
]);

// Or 
 
$sarlaftSearch = new PlacetoPay\SarlaftClient\SarlafSearchMultiburo();
$sarlaftSearch->setIdIdentificationType('');
$sarlaftSearch->setIdentificationNumber('');
$sarlaftSearch->setDenomination('');
$sarlaftSearch->setTag(''); //optional
$sarlaftSearch->setTimeOut(''); //optional

$result = $gateway->consult($sarlaftSearch);

if (!$result->isSuccessful()) {
    $result->message(); //return a string with error information
}

$result->isRisk(); //return a boolean value
$result->getScore(); //return a integer value bethwen 0 and 100
$result->message(); //return a string indicating if the search found information
$result->getAdditionalData(); //return an arrangement with the information collected from the search 
```

* Request for Rues

```php

$data = [
            'document' => '',
            'documentType' => '',
            'denomination' => '', //optional
            'timeOut' => '', //optional
            'tag' => '', //optional
        ];

$result = $gateway->searchRues($data);

if (!$result->isSuccessful()) {
    $result->status()->message(); //return a string with error information
}

$result->isSuccessful(); //return a boolean value
$result->status(); //return the Status
$result->status()->message(); //return a string indicating if the search found information
$result->ruesData(); //return the result of Rues


```

* Request for Basic data

```php

$data = [
            'document' => '',
            'documentType' => '',
            'denomination' => '', //optional
            'timeOut' => '', //optional
            'tag' => '', //optional
        ];

$result = $gateway->searchBasicData($data);

if (!$result->isSuccessful()) {
    $result->status()->message(); //return a string with error information
}

$result->isSuccessful(); //return a boolean value
$result->status(); //return the Status
$result->status()->message(); //return a string indicating if the search found information
$result->basicData(); //return the result of Basic data service


```

* Possible reasons if it is not approved
1. The data sent does not comply with the format or possible defined values
2. Multiburó service changed the structure of your response
3. Connection failed with Multiburó service
4. No mandatory information sent
* Notes:
1. The Denomination field allows you to search by the person's name regardless of the word order
2. The Tag field allows you to record some detail and then subdivide the billing,
   for example, the area that makes the query could be sent there
3. The TimeOut field defines the waiting time for updating the
   source, expressed in seconds.
4. The possible values for the type of identification are: 1 Citizenship Card, 2 NIT 3, Foreigner Card, 5 passport, 6 Without Identification and 8 Foreign Document

## Testing with the mock responses

### Integration

When integrating the SDK with other applications it is necessary to carry out test queries, therefore the SDK includes a mock for the client that must be passed when creating an instance and this mock has defined a set of responses that will be delivered according to the document sent in the petition

In order to access the mock together with its answers, it is necessary to modify the autoload-dev key in the composer.json, here we are going to add the following lines

```json
 "autoload-dev": {
        "classmap": [
            "vendor/placetopay/sarlaft-client/tests"
        ]
 },
```
The above in order to be able to map the namespaces with their corresponding routes for the mock

Once this is done you must run `composer dump-autoload`

Now you can use the SoapCarrierMock class with the namespace PlacetoPay\SarlaftClient\Tests\Cases\SoapCarrierMock as the client.

```php
$settings = [
    ...
    'client' => (new \PlacetoPay\SarlaftClient\Tests\Cases\SoapCarrierMock(\PlacetoPay\SarlaftClient\Gateways\Multiburo::WDSL)),
    ...
];
```

When making the request, you must send a value for the identificationNumber field, the possible values to send and the response you will get are listed below:


* `1061111111` A response will be obtained where matches were found in news, restrictive lists and in PEP's, for all the previous identity represents a risk
* `1061111112` A response will be obtained where no coincidences were found, therefore the identity does not represent a risk
* `1061111113` A response with a FAILED status will be obtained because the authentication data is wrong.
* `1061111114` A response with a FAILED status because the authentication data matches an existing but inactive user.
* `1061111115` A response with a FAILED status will be obtained due to the service responding with an unknown structure.
* `1061111116` A response with a FAILED status due to an inconsistency in any of the values sent.