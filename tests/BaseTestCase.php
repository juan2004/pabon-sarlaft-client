<?php

namespace Pabon\SarlaftClient\Tests;

use PHPUnit\Framework\TestCase;
use Pabon\SarlaftClient\Gateway;

class BaseTestCase extends TestCase
{
    public function gateway(): Gateway
    {
        return (new Gateway([
            'location' => 'https://ws2.grupojurad.com:5310/App/ClienteGrupoJuradWs',
            'username' => 'someusername',
            'password' => 'somepassword',
            'internalUser' => null,
        ]))->mock();
    }
}
