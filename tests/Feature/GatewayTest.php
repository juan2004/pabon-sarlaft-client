<?php

namespace Pabon\SarlaftClient\Tests\Feature;

use Pabon\SarlaftClient\Entities\RiskQuery\Result;
use Pabon\SarlaftClient\Entities\SearchBasicData\ResultBasicData;
use Pabon\SarlaftClient\Entities\SearchIdState\ResultIdState;
use Pabon\SarlaftClient\Entities\SearchRues\ResultRues;
use Pabon\SarlaftClient\Tests\BaseTestCase;

class GatewayTest extends BaseTestCase
{
    protected function makeQuery($document, $documentType = 'CC', array $overrides = []): Result
    {
        $gateway = $this->gateway();

        return $gateway->riskQuery(array_merge([
            'document' => $document,
            'documentType' => $documentType,
        ], $overrides));
    }

    protected function makeSearchRues($document, $documentType = 'NIT', array $overrides = []): ResultRues
    {
        $gateway = $this->gateway();

        return $gateway->searchRues(array_merge([
            'document' => $document,
            'documentType' => $documentType,
        ], $overrides));
    }

    protected function makeSearchBasicData($document, $documentType = 'CC', array $overrides = []): ResultBasicData
    {
        $gateway = $this->gateway();

        return $gateway->searchBasicData(array_merge([
            'document' => $document,
            'documentType' => $documentType,
        ], $overrides));
    }

    protected function makeSearchIdState(
        $document,
        $documentIssueDate = '28/02/2022',
        $documentType = 'CC',
        array $overrides = []
    ): ResultIdState
    {
        $gateway = $this->gateway();

        return $gateway->searchIdState(array_merge([
            'document' => $document,
            'documentType' => $documentType,
            'documentIssueDate' => $documentIssueDate,
        ], $overrides));
    }

    public function testItBringsTheSearchRuesData()
    {
        $result = $this->makeSearchRues('1061111111', 'NIT');

        $this->assertTrue($result->isSuccessful());
        $this->assertGreaterThan(1, count($result->ruesData()));
    }

    public function testTheSearchRuesHandlesAnInnocentResponse()
    {
        $result = $this->makeSearchRues('1061111112');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(ResultRues::RESULT_WITHOUT_INFO, $result->status()->message());
        $this->assertEmpty($result->ruesData()['enrollments']);
    }

    public function testTheSearchRuesHandlesAnEmptyResponse()
    {
        $result = $this->makeSearchRues('1040035000');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(ResultRues::RESULT_WITHOUT_INFO, $result->status()->message());
        $this->assertEmpty($result->ruesData()['enrollments']);
    }

    public function testTheSearchRuesHandlesAnAuthenticationError()
    {
        $result = $this->makeSearchRues('1061111113');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(ResultRues::RESULT_WITHOUT_INFO, $result->status()->message());
        $this->assertEquals(0, $result->ruesData()['status']);
        $this->assertEquals('502', $result->ruesData()['errorCode']);
        $this->assertEquals('USUARIO INVÁLIDO', $result->ruesData()['errorMessage']);
        $this->assertEmpty($result->ruesData()['enrollments']);
    }

    public function testTheSearchRuesReturnsTheErrorMessage()
    {
        $result = $this->makeSearchRues('1061111114');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('Usuario inactivo', $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->ruesData());
    }

    public function testTheSearchRuesHandlesAnIncorrectResponse()
    {
        $result = $this->makeSearchRues('1061111116');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('The formatter threw an exception while trying to deserialize the message: There'.
            ' was an error while trying to deserialize parameter http://ws.grupojurad.com:busqueda. The InnerException'.
            " message was 'There was an error deserializing the object of type App.Busqueda. The value '' cannot be".
            " parsed as the type 'Int64'.'.  Please see InnerException for more details.", $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->ruesData());
    }

    public function testTheSearchRuesHandlesABadStructureResponse()
    {
        $result = $this->makeSearchRues('1061111115');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('The formatter threw an exception while trying to deserialize the message: Error'.
            " in deserializing body of request message for operation 'buscarRue'. Start element 'buscarRue' does not".
            " match end element 'Envelope'. Line 14, position 11.", $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->ruesData());
    }

    public function testItBringsTheRiskQueryData()
    {
        $result = $this->makeQuery('1061111111', 'CC');

        $this->assertTrue($result->isSuccessful());
        $this->assertTrue($result->hasRisk());
        $this->assertEquals(100, $result->score());
        $this->assertGreaterThan(1, count($result->riskData('peps')));
        $this->assertGreaterThan(1, count($result->riskData('news')));
        $this->assertGreaterThan(1, count($result->riskData('lists')));
    }

    public function testItHandlesAnInnocentResponse()
    {
        $result = $this->makeQuery('1061111112');

        $this->assertTrue($result->isSuccessful());
        $this->assertFalse($result->hasRisk());
        $this->assertEquals(0, $result->score());
    }

    public function testItHandlesAnEmptyResponse()
    {
        $result = $this->makeQuery('1040035000');

        $this->assertTrue($result->isSuccessful());
        $this->assertFalse($result->hasRisk());
        $this->assertEquals(0, $result->score());
        $this->assertEquals(0, count($result->riskData('peps')));
    }

    public function testItHandlesAnAuthenticationError()
    {
        $result = $this->makeQuery('1061111113');

        $this->assertFalse($result->isSuccessful());
        $this->assertFalse($result->hasRisk());
        $this->assertEquals(-1, $result->score());
        $this->assertEmpty($result->riskData());
    }

    public function testItReturnsTheErrorMessage()
    {
        $result = $this->makeQuery('1061111114');
        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('Usuario inactivo', $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
    }

    public function testItHandlesAnIncorrectResponse()
    {
        $result = $this->makeQuery('1061111116');
        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('Nullable object must have a value.', $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
    }

    public function testItHandlesABadStructureResponse()
    {
        $result = $this->makeQuery('1061111115');
        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('Object reference not set to an instance of an object.', $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
    }

    public function testItBringsTheSearchBasicData()
    {
        $result = $this->makeSearchBasicData('1061111111', 'NIT');

        $this->assertTrue($result->isSuccessful());
        $this->assertGreaterThan(1, count($result->basicData()));
    }

    public function testTheSearchBasicDataHandlesAnInnocentResponse()
    {
        $result = $this->makeSearchBasicData('1061111112');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(
            \Pabon\SarlaftClient\Constants\Result::RESULT_WITHOUT_INFO,
            $result->status()->message()
        );
        $this->assertEmpty($result->basicData()['denomination']);
        $this->assertEmpty($result->basicData()['idStatus']);
    }

    public function testTheSearchBasicDataHandlesAnEmptyResponse()
    {
        $result = $this->makeSearchBasicData('1040035000');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(
            \Pabon\SarlaftClient\Constants\Result::RESULT_WITHOUT_INFO,
            $result->status()->message()
        );
        $this->assertEmpty($result->basicData()['denomination']);
        $this->assertEmpty($result->basicData()['idStatus']);
    }

    public function testTheSearchBasicDataHandlesAnAuthenticationError()
    {
        $result = $this->makeSearchBasicData('1061111113');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(
            \Pabon\SarlaftClient\Constants\Result::RESULT_WITHOUT_INFO,
            $result->status()->message()
        );
        $this->assertEquals(0, $result->basicData()['status']);
        $this->assertEquals('504', $result->basicData()['errorCode']);
        $this->assertEquals('PASSWORD INVÁLIDO', $result->basicData()['errorMessage']);
        $this->assertEmpty($result->basicData()['denomination']);
        $this->assertEmpty($result->basicData()['idStatus']);
    }

    public function testTheSearchBasicDataReturnsTheErrorMessage()
    {
        $result = $this->makeSearchBasicData('1061111114');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('Usuario inactivo', $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->basicData());
    }

    public function testTheSearchBasicDataHandlesAnIncorrectResponse()
    {
        $result = $this->makeSearchBasicData('1061111116');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('The formatter threw an exception while trying to deserialize the message: There'.
            ' was an error while trying to deserialize parameter http://ws.grupojurad.com:busqueda. The InnerException'.
            " message was 'There was an error deserializing the object of type App.Busqueda. The value '' cannot be".
            " parsed as the type 'Int64'.'.  Please see InnerException for more details.", $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->basicData());
    }

    public function testTheSearchBasicDataHandlesABadStructureResponse()
    {
        $result = $this->makeSearchBasicData('1061111115');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals("Error in deserializing body of request message for operation 'buscarDatosBasicos'.".
            " OperationFormatter encountered an invalid Message body. Expected to find node type 'Element' with name".
            " 'buscarDatosBasicos' and namespace 'http://ws.grupojurad.com'. Found node type 'Element' with name".
            " 'Usuario' and namespace 'http://schemas.datacontract.org/2004/07/App'", $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->basicData());
    }

    public function testItBringsTheSearchIdState()
    {
        $result = $this->makeSearchIdState('1061111111');

        $this->assertTrue($result->isSuccessful());
        $this->assertGreaterThan(1, count($result->idState()));
    }

    public function testTheSearchIdStateHandlesAnInnocentResponse()
    {
        $result = $this->makeSearchIdState('1061111112');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(ResultRues::RESULT_WITHOUT_INFO, $result->status()->message());
        $this->assertEmpty($result->idState()['idStatus']);
    }

    public function testTheSearchIdStateHandlesAnEmptyResponse()
    {
        $result = $this->makeSearchIdState('1040035000');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(ResultRues::RESULT_WITHOUT_INFO, $result->status()->message());
        $this->assertEmpty($result->idState()['idStatus']);
    }

    public function testTheSearchIdStateHandlesAnAuthenticationError()
    {
        $result = $this->makeSearchIdState('1061111113');

        $this->assertTrue($result->isSuccessful());
        $this->assertEquals('00', $result->status()->reason());
        $this->assertEquals(ResultRues::RESULT_WITHOUT_INFO, $result->status()->message());
        $this->assertEquals(0, $result->idState()['status']);
        $this->assertEquals('504', $result->idState()['errorCode']);
        $this->assertEquals('PASSWORD INVÁLIDO', $result->idState()['errorMessage']);
        $this->assertEmpty($result->idState()['idStatus']);
    }

    public function testTheSearchIdStateReturnsTheErrorMessage()
    {
        $result = $this->makeSearchIdState('1061111114');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('Usuario inactivo', $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->idState());
    }

    public function testTheSearchIdStateHandlesAnIncorrectResponse()
    {
        $result = $this->makeSearchIdState('1061111116');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('The formatter threw an exception while trying to deserialize the message: There'.
            ' was an error while trying to deserialize parameter http://ws.grupojurad.com:busqueda. The InnerException'.
            " message was 'There was an error deserializing the object of type App.Busqueda. The value '' cannot be".
            " parsed as the type 'Int64'.'.  Please see InnerException for more details.", $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->idState());
    }

    public function testTheSearchIdStateHandlesABadStructureResponse()
    {
        $result = $this->makeSearchIdState('1061111115');

        $this->assertFalse($result->isSuccessful());
        $this->assertEquals('The formatter threw an exception while trying to deserialize the message: Error '.
            "in deserializing body of request message for operation 'buscarEstadoCedula'. Unexpected end of file. Following ".
            'elements are not closed: buscarEstadoCedula, Body, Envelope. Line 13, position 7.', $result->status()->message());
        $this->assertEquals('XE', $result->status()->reason());
        $this->assertEmpty($result->idState());
    }
}
